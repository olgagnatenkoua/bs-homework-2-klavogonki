# BSA 2019 JS - Homework 2

Klavogonki

# Start the server

To start the server, run

```javascript
nodemon server.js
```

# Open Start Page

To start working with the app, go to http://localhost:3000 in your browser
You can use the following user to login:

```javascript
{
    "login": "user1",
    "email": "user1@gmail.com",
    "password": "user1"
}
```

### Note
English texts sourced from this resource: http://klavogonki.ru/vocs/5539/
