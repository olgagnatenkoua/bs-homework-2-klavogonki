window.onload = () => {
  const jwt = localStorage.getItem("jwt");
  jwt ? location.replace("/race") : location.replace("/login");
};

config
