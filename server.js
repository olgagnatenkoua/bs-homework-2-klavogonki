const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);

const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");

const users = require("./users");
const texts = require("./texts");
const config = require("./config");
require("./passport.config");

app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

server.listen(3000);

/* provide resources */

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.get("/race", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "race.html"));
});

app.get("/login", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "login.html"));
});

/* handle routes */

// protected route
app.get("/text", passport.authenticate("jwt"), (req, res) => {
  res.status(200).send({
    gameText
  });
});

app.post("/login", function(req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.email);

  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, config.SECRET);
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

//   io.on('connection', socket => {
//     socket.on('submitMessage', payload => {
//       const { message, token } = payload;
//       const userLogin = jwt.decode(token).login;
//       socket.broadcast.emit('newMessage', { message, user: userLogin });
//       socket.emit('newMessage', { message, user: userLogin });
//     });

/* prepare game setting */

let startedWait = false;
let startedGame = 0; // date and time when game was started
let waitInterval = null;
let gameTimeout = null;
let waitTime = 0;
let gameText = {
  textId: 0,
  text: texts[0]
};
let gameHappened = false;
let players = {};
// player contract: {correctRate: 71, errorRate: 40, timeSpent: 2500, disconnected: false, finished: false }

// manage game cycles
const selectText = require("./select-text.js");

/* process connections and events */

io.on("connection", socket => {
  socket.join("waiting");

  // init game wait if this is first connected user
  if (!startedWait && !startedGame) {
    startWait();
  }

  if (startedGame) {
    socket.emit("busy");
  }

  socket.on("disconnect", () => {
    if (players[socket.id]) {
      players[socket.id].disconnected = true;
    }
    const gameRoom = io.sockets.adapter.rooms["game"];
    const hasActivePlayers = gameRoom && gameRoom.length;
    if (!hasActivePlayers) {
      const mandatoryFinish = true;
      endGame(mandatoryFinish);
    }
    socket.removeAllListeners();
  });

  socket.on("accept-start", payload => {
    const user = jwt.verify(payload.token, config.SECRET);
    if (user) {
      socket.leave("waiting");
      players[socket.id].user.email = user.email;
      socket.join("game");
    } else {
      // user is unauthorized and is not a player
      delete players[socket.id];
    }
  });

  socket.on("accept-finish", payload => {
    const user = jwt.verify(payload.token, config.SECRET);
    if (user) {
      socket.leave("game");
      socket.join("waiting");
    }
  });

  socket.on("update", payload => {
    handleUpdateEvt(payload);
  });
});

handleUpdateEvt = payload => {
  const { token, correctSymbols, errorSymbols, id, finished } = payload;
  const user = jwt.verify(token, config.SECRET);
  if (user) {
    players[id].correctRate =
      Math.floor((correctSymbols / gameText.text.length) * 100) / 100;
    players[id].errorRate =
      Math.floor((errorSymbols / gameText.text.length) * 100) / 100;
    players[id].finished = finished;
    io.to("game").emit("progress", {
      players
    });
    const allFinished = Object.keys(players).every(id => players[id].finished);
    if (allFinished) {
      endGame();
    }
  }
};

setPlayerList = () => {
  const waitingPlayers = io.sockets.adapter.rooms["waiting"].sockets;
  Object.keys(waitingPlayers).forEach(id => {
    players[id] = {
      id,
      user: {
        email: ""
      },
      correctRate: 0,
      correctSymbols: 0,
      finished: false,
      disconnected: false
    };
  });
};

startGame = () => {
  if (!startedGame) {
    // temporary solution to compensate the issue with clearInterval
    startedWait = false;
    console.log("STARTING GAME");
    startedGame = Date.now();
    gameTimeout = setTimeout(endGame, config.GAME_DURATION);
    setPlayerList();
    io.to("waiting").emit("start", {
      timeToGame: config.GAME_DURATION,
      players
    });
  }
};

startWait = () => {
  if (gameHappened) {
    gameText = selectText(gameText.textId);
  }
  startedWait = true;
  const timeUntilGameEnd = startedGame
    ? config.GAME_DURATION - (Date.now() - startedGame)
    : 0;
  waitTime = config.TIME_BETWEEN_GAMES + timeUntilGameEnd;
  waitInterval = setInterval(updateWait, config.WAIT_EVENT_FREQUENCY);
};

updateWait = () => {
  io.to("waiting").emit("wait", {
    timeToGame: waitTime
  });
  if (waitTime >= 0) {
    waitTime = waitTime - config.WAIT_EVENT_FREQUENCY;
  } else {
    waitTime = 0;
    clearInterval(waitInterval); // todo: fix issue with setInterval not being cleared
    waitInterval = null;
    startGame();
  }
};

endGame = (mandatory = false) => {
  // check if game has any remaining time before end
  const timeDiff = config.GAME_DURATION - (Date.now() - startedGame);
  if (timeDiff > 0) {
    // reduce waiting time for "waiting" room by the difference
    timeToGame = timeDiff + config.TIME_BETWEEN_GAMES;
    io.to("waiting").emit("wait", {
      timeToGame
    });
  }

  startedWait = false;
  startedGame = 0;
  clearTimeout(gameTimeout);

  if (mandatory) {
    // game ended because no players were connected
    gameHappened = false;
    players = {};
    return;
  }

  gameHappened = true;
  timer = null;

  io.to("game").emit("finish", {
    players,
    gameText
  });
  startWait();
};

// set game updates from server side - check that players are alive, otherwise stop game
